<?php

namespace Quis\Ptsz\DataManagment;

use Quis\Ptsz\Data\Instance;
use Quis\Ptsz\Data\Job;

class DataLoader
{
    public function load(string $filename): LoadedData {
        $fileContents = file_get_contents($filename);
        $fileContents = preg_replace('/\s+/', ' ', trim($fileContents));

        $instanceNum = 0;
        $jobNum = 1;
        $loadedData = new LoadedData();
        $loadedData->setFileName($filename);

        $fileContentsArray = explode(' ', $fileContents);
        $tmpArray = [];

        $instance = null;

        foreach($fileContentsArray as $idx => $val) {
            if(!$idx) {
                $loadedData->setPlannedInstances(intval($val));
            } else {
                if(empty($instance)) {
                    $instance = new Instance(++$instanceNum);
                    $instance->setPlannedJobsCount(intval($val));
                    $loadedData->addInstance($instance);
                } else {
                    $tmpArray[] = intval($val);
                    if(count($tmpArray) === 3) {
                        $job = new Job($jobNum++, $tmpArray[0], $tmpArray[1], $tmpArray[2]);
                        $instance->add($job);
                        $tmpArray = [];

                        if($instance->getJobsCount() === $instance->getPlannedJobsCount()) {
                            unset($instance);
                            $jobNum = 1;
                        }
                    }
                }
            }
        }
        return $loadedData;
    }
}