<?php

namespace Quis\Ptsz\DataManagment;


use Quis\Ptsz\Exceptions\EmptyResultFileException;
use Quis\Ptsz\Exceptions\ValidationException;
use Quis\Ptsz\Utils\FileNameCleaner;

class ResultReader
{
    public function read(string $fileName): LoadedResult {
        $contents = file_get_contents($fileName);
        if(empty($contents)) {
            throw new EmptyResultFileException("File $fileName not found");
        }
        $result = new LoadedResult();
        $cleanedFileName = FileNameCleaner::truncateFileExtensionAndPath($fileName);
        $cleanedFileNameArray = explode('_', $cleanedFileName);

        $contents = preg_replace('/[^0-9 .]+/', '', trim($contents));
        $contentsArray = explode(' ', $contents);

        $result
            ->setSourceFileName($cleanedFileNameArray[0] .'.txt')
            ->setHCoefficient((float) (floatval($cleanedFileNameArray[1]) / 10))
            ->setInstanceNum(intval($cleanedFileNameArray[2]))
            ->setPenaltiesSum(intval(array_shift($contentsArray)))
            ->setStartTime(intval(array_shift($contentsArray)))
            ->setCommonDueDate(intval(array_shift($contentsArray)));

        $jobsList = [];
        $num = 0;
        $lastIdx = -1;
        foreach($contentsArray as $jobVal) {
            if(!$num) {
                $lastIdx = array_push($jobsList, [])-1;
            }
            $jobsList[$lastIdx][] = intval($jobVal);
            $num++;
            if($num > 2) {
                $num = 0;
            }
        }
        $result->setJobsList($jobsList);

        return $result;
    }
}