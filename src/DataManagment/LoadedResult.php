<?php

namespace Quis\Ptsz\DataManagment;


class LoadedResult
{
    protected $sourceFileName;
    protected $hCoefficient;
    protected $instanceNum;
    protected $penaltySum;
    protected $startTime;
    protected $commonDueDate;
    protected $jobsList;

    public function setSourceFileName(string $sourceFileName): self {
        $this->sourceFileName = $sourceFileName;
        return $this;
    }
    public function getSourceFileName(): string {
        return $this->sourceFileName;
    }
    public function setHCoefficient(float $hCoefficient): self {
        $this->hCoefficient = $hCoefficient;
        return $this;
    }
    public function getHCoefficient(): float {
        return $this->hCoefficient;
    }
    public function setInstanceNum(int $instanceNum): self {
        $this->instanceNum = $instanceNum;
        return $this;
    }
    public function getInstanceNum(): int {
        return $this->instanceNum;
    }
    public function setPenaltiesSum(int $penaltySum): self {
        $this->penaltySum = $penaltySum;
        return $this;
    }
    public function getPenaltiesSum(): int {
        return $this->penaltySum;
    }
    public function setStartTime(int $startTime): self {
        $this->startTime = $startTime;
        return $this;
    }
    public function getStartTime(): int {
        return $this->startTime;
    }
    public function setCommonDueDate(int $commonDueDate): self {
        $this->commonDueDate = $commonDueDate;
        return $this;
    }
    public function getCommonDueDate(): int {
        return $this->commonDueDate;
    }
    public function setJobsList(array $jobsList): self {
        $this->jobsList = $jobsList;
        return $this;
    }
    public function getJobsList(): array {
        return $this->jobsList;
    }
    public function getJobsCount(): int {
        return count($this->jobsList);
    }
}