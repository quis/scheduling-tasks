<?php

namespace Quis\Ptsz\DataManagment;

use Quis\Ptsz\Data\Instance;
use Quis\Ptsz\Exceptions\InstanceNotFoundException;

class LoadedData
{
    protected $instances = [];
    protected $plannedInstances;
    protected $fileName;

    public function addInstance(Instance $instance) {
        $this->instances[] = $instance;
    }
    public function getInstances(): array {
        return $this->instances;
    }
    public function getInstanceByNum(int $num): Instance {
        foreach($this->getInstances() as $instance) {
            if($instance->getNum() === $num) {
                return $instance;
            }
        }
        $ex = new InstanceNotFoundException("Instance with id $num not found");
        $ex->setInstanceNum($num);
        throw $ex;
    }
    public function getInstancesCount() {
        return count($this->getInstances());
    }
    public function getPlannedInstances() {
        return $this->plannedInstances;
    }
    public function setPlannedInstances(int $plannedInstances) {
        $this->plannedInstances = $plannedInstances;
        return $this;
    }
    public function getFileName(): string {
        return $this->fileName;
    }
    public function setFileName(string $fileName): self {
        $this->fileName = $fileName;
        return $this;
    }
}