<?php

namespace Quis\Ptsz\Validator;


use Quis\Ptsz\Configuration;
use Quis\Ptsz\Data\Timeline;
use Quis\Ptsz\DataManagment\DataLoader;
use Quis\Ptsz\DataManagment\LoadedData;
use Quis\Ptsz\DataManagment\LoadedResult;
use Quis\Ptsz\Exceptions\InstanceNotFoundException;
use Quis\Ptsz\Exceptions\JobNotFoundException;
use Quis\Ptsz\Exceptions\ValidationException;
use Quis\Ptsz\Utils\FileNameResolver;

class Validator
{
    protected $dataCache;
    protected $resolver;

    public function __construct(FileNameResolver $resolver) {
        $this->resolver = $resolver;
    }

    /**
     * @param LoadedResult $result loaded result
     * @throws ValidationException if an validation error occurs
     */
    public function validate(LoadedResult $result) {
        $path = $this->resolver->getDataFile($result->getSourceFileName());
        $loadedData = $this->getDataFromCache($path);
        if(!isset($loadedData)) {
            $loader = new DataLoader();
            $loadedData = $loader->load($path);
            $this->saveDataToCache($path, $loadedData);
        }

        try {
            $instance = $loadedData->getInstanceByNum($result->getInstanceNum());
        } catch(InstanceNotFoundException $ex) {
            throw new ValidationException("Instance with id ". $ex->getInstanceNum() ." not found");
        }

        if($instance->getJobsCount() !== $result->getJobsCount()) {
            throw new ValidationException("There should be ". $instance->getJobsCount() ." jobs, found ". $result->getJobsCount() ." jobs");
        }

        $commonDueDate = $instance->getCommonDueDate($result->getHCoefficient());
        if($result->getCommonDueDate() !== $commonDueDate) {
            throw new ValidationException("Common due date ". $result->getCommonDueDate() ." is invalid! Valid common due date: ". $commonDueDate);
        }

        $orderedJobList = [];
        try {
            foreach ($result->getJobsList() as $jobData) {
                $orderedJobList[] = $instance->getJobByData($jobData);
            }
        } catch(JobNotFoundException $ex) {
            throw new ValidationException("Job with data ". $ex->getDataAsString() ." not found in instance k=".$result->getInstanceNum() ."!");
        }

        $startTime = $result->getStartTime();
        $timeline = new Timeline();
        $timeline->setCommonDueDate($result->getCommonDueDate());

        foreach($orderedJobList as $job) {
            $timeline->setJob($startTime, $job);
            $startTime += $job->getProcessTime();
        }

        if($timeline->getPenaltiesSum() !== $result->getPenaltiesSum()) {
            throw new ValidationException("Invalid penalties sum calculated! Should be ". $timeline->getPenaltiesSum() .", but in result found ". $result->getPenaltiesSum());
        }
    }

    protected function getDataFromCache(string $sourceFileName): ?LoadedData {
        if(isset($this->dataCache[$sourceFileName])) {
            return $this->dataCache[$sourceFileName];
        }
        return null;
    }

    protected function saveDataToCache(string $sourceFileName, LoadedData $data): self {
        $this->dataCache[$sourceFileName] = $data;
        return $this;
    }
}