<?php
/**
 * Created by PhpStorm.
 * User: dawid
 * Date: 29.11.2018
 * Time: 18:08
 */

namespace Quis\Ptsz\Exceptions;


class InstanceNotFoundException extends \Exception
{
    protected $instanceNum;

    public function setInstanceNum(int $instanceId): self {
        $this->instanceNum = $instanceId;
        return $this;
    }

    public function getInstanceNum(): int {
        return $this->instanceNum;
    }
}