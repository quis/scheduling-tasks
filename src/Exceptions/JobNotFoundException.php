<?php

namespace Quis\Ptsz\Exceptions;

class JobNotFoundException extends \Exception
{
    /**
     * @var int
     */
    protected $jobId;
    /**
     * @var array
     */
    protected $jobData;

    public function setJobId(int $jobId): self {
        $this->jobId = $jobId;
        return $this;
    }

    public function getJobId(): int {
        return $this->jobId;
    }

    public function setJobData(array $data): self {
        $this->jobData = $data;
        return $this;
    }

    public function getJobDataAsString(): string {
        return implode(' ', $this->jobData);
    }
}