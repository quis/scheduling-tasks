<?php

namespace Quis\Ptsz\Utils;

/**
 * Class Permutation
 * @package Quis\Ptsz\Utils
 * Source: https://stackoverflow.com/questions/5506888/permutations-all-possible-sets-of-numbers
 */
class Permutation {
    private $result;

    public function getResult() {
        return $this->result;
    }

    public function permute(array $source, array $permutated = []) {
        if (empty($permutated)){
            $this->result = array();
        }
        if (empty($source)){
            $this->result[] = $permutated;
        } else {
            $count = count($source);
            for($i = 0; $i<$count; $i++) {
                $new_permutated = $permutated;
                $new_permutated[] = $source[$i];
                $new_source = array_merge(array_slice($source,0,$i), array_slice($source,$i+1));
                $this->permute($new_source, $new_permutated);
            }
        }
        return $this;
    }
}