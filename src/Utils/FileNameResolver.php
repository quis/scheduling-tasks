<?php

namespace Quis\Ptsz\Utils;


use Quis\Ptsz\Configuration;
use Quis\Ptsz\Exceptions\AlgorithmNotFoundException;
use Quis\Ptsz\Exceptions\DataFileNotFoundException;

class FileNameResolver
{
    protected $config;

    public function __construct(Configuration $config) {
        $this->config = $config;
    }

    public function getDataFile(string $dataFile): string {
        $dataPath = $this->config->getParam('data_path');
        $fileName = "$dataPath/$dataFile";
        if(!file_exists($fileName)) {
            throw new DataFileNotFoundException("Data file $dataFile not found. Checked location: $fileName");
        }
        return $fileName;
    }

    public function guessAlgorithmClass(string $algorithm) {
        $className = strtolower($algorithm) .'algorithm';
        $potentialClassFile = $className .'.php';
        $path = $this->config->getParam('algorithms_path');
        $availableClasses = scandir($path);
        $namespace = $this->config->getParam('algorithms_namespace');
        if($availableClasses) {
            foreach($availableClasses as $classFile) {
                if($classFile === '.' || $classFile === '..') {
                    continue;
                }
                if(strtolower($classFile) === $potentialClassFile) {
                    $classFileArray = explode('.', $classFile);
                    array_pop($classFileArray);
                    return $namespace . implode('.', $classFileArray);
                }
            }
        }
        throw new AlgorithmNotFoundException("Algorithm $algorithm not found, checked in directory $path for names like $potentialClassFile");
    }

    public function getResultsFiles(string $query): array {
        $results = [];
        $path = $this->config->getParam('results_path');
        $query = strtolower($query);
        $availableFiles = scandir($path);
        $queryLen = strlen($query);
        foreach($availableFiles as $fileName) {
            if($fileName === '.' || $fileName === '..') {
                continue;
            }
            $fileNameCut = strtolower(substr($fileName, 0, $queryLen));
            if($fileNameCut === $query) {
                $results[] = $path .'/'. $fileName;
            }
        }
        return $results;
    }
}