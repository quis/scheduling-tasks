<?php

namespace Quis\Ptsz\Utils;


class FileNameCleaner
{
    public static function truncateFileExtensionAndPath(string $fileName): string {
        $fileNameArray = explode('.', $fileName);
        array_pop($fileNameArray);
        $fileNameArray = explode('/', implode('.', $fileNameArray));
        return array_pop($fileNameArray);
    }
}