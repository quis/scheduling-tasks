<?php

namespace Quis\Ptsz\Algorithms;


use Quis\Ptsz\Algorithms\Abstracts\Algorithm;
use Quis\Ptsz\Configuration;
use Quis\Ptsz\Data\Result;
use Quis\Ptsz\Output\ResultSetOutputter;
use Quis\Ptsz\Utils\Permutation;

class BruteForceAlgorithm extends Algorithm
{
    public function process(): Result
    {
        $due = $this->instance->getCommonDueDate($this->h);
        $permutation = new Permutation();
        $permutations = $permutation->permute($this->instance->getJobList())->getResult();

        $lowestPenaltyResult = null;
        $lowestPenaltySum = -1;

        $permutationsCount = count($permutations);
        $outputter = new ResultSetOutputter(Configuration::getInstance());
        $outputter->setSourceFile('bruteforce'. $this->instance->getPlannedJobsCount() .'.txt');

        $maxStartTime = (int) ceil($due*0.6);
        for($startTime = 0; $startTime < $maxStartTime; $startTime++) {
            foreach($permutations as $idx => $jobList) {
                print("Permutation $idx/$permutationsCount for startTime $startTime...");
                $currentTime = $startTime;
                $this->prepareResultAndTimeline();

                foreach($jobList as $job) {
                    $this->timeline->setJob($currentTime, $job);
                    $currentTime += $job->getProcessTime();
                }

                $sum = $this->timeline->getPenaltiesSum();
                print(" sum $sum");
                if($lowestPenaltySum === -1 || $sum < $lowestPenaltySum) {
                    print(" I take it!");
                    $lowestPenaltyResult = $this->result;
                    $lowestPenaltySum = $sum;
                    $outputter->outputResultToFile($lowestPenaltyResult, $outputter->getFileNameForResult($lowestPenaltyResult));
                } else {
                    print(" I reject! Current lowest: ". $lowestPenaltySum);
                    unset($this->timeline);
                    unset($this->result);
                }
                print(PHP_EOL);
            }
        }

        return $lowestPenaltyResult;
    }
}