<?php

namespace Quis\Ptsz\Algorithms;

use Quis\Ptsz\Algorithms\Abstracts\Algorithm;
use Quis\Ptsz\Data\Job;
use Quis\Ptsz\Data\Result;
use Quis\Ptsz\Exceptions\AlgorithmException;

class MyAlgorithm extends Algorithm
{
    public function process(): Result
    {
        $due = $this->instance->getCommonDueDate($this->h);
        $sortedJobsLeft = $this->sortByHeurestics("firstTryLeft");
        $sortedJobsRight = $this->sortByHeurestics("firstTryRight");

        $endTime = (int) ceil($due*0.3);
        $breakpoint = (int) ceil($due*0.23);
        $lowestSum = -1;
        $lowestResult = null;

        for ($startTime = 0; $startTime < $endTime; $startTime++) {
            if($startTime > 50) {
                $startTime++;
            }
            if($startTime > $breakpoint) {
                $startTime++;
            }

            $this->prepareResultAndTimeline();
            $leftIndex = 0;
            $rightIndex = 0;
            $useRight = false;
            $jobsCount = $this->getInstance()->getJobsCount();
            $addedJobs = [];
            $lastAddedJob = null;

            while (true) {
                if(isset($lastAddedJob)) {
                    $nextStartTime = $lastAddedJob->getEndTime();
                } else {
                    $nextStartTime = $startTime;
                }

                if (!$useRight) {
                    $job = $sortedJobsLeft[$leftIndex++]['job'];
                    if(!isset($addedJobs[$job->getId()])) {
                        $lastAddedJob = $this->timeline->setJob($nextStartTime, $job);
                        $addedJobs[$job->getId()] = true;
                        if ($lastAddedJob->getEndTime() >= $due) {
                            $useRight = true;
                        }
                    }
                    if($leftIndex >= $jobsCount) {
                        break;
                    }
                } else {
                    $job = $sortedJobsRight[$rightIndex++]['job'];
                    if(!isset($addedJobs[$job->getId()])) {
                        $lastAddedJob = $this->timeline->setJob($nextStartTime, $job);
                        $addedJobs[$job->getId()] = true;
                    }
                    if($rightIndex >= $jobsCount) {
                        break;
                    }
                }
            }

            if($this->isNotUsedJobsLeft()) {
                throw new AlgorithmException("Not used jobs left after main loop :(");
            }

            $sum = $this->timeline->getPenaltiesSum();
            if($lowestSum === -1 || $sum < $lowestSum) {
                $lowestSum = $sum;
                $lowestResult = $this->result;
            }
        }
        return $lowestResult;
    }

    protected function firstTryLeft(Job $job): float {
        $processTime = $job->getProcessTime();
        $earlinessPenalty = $job->getEarlinessPenalty();
        $tardinessPenalty = $job->getTardinessPenalty();
        $processTimeWeight = -3;
        $earlinessPenaltyWeight = 10;
        $tardinessPenaltyWeight = -5;
        return (($processTimeWeight*$processTime)+($earlinessPenaltyWeight*$earlinessPenalty)+($tardinessPenaltyWeight*$tardinessPenalty))/(abs($processTimeWeight)+abs($earlinessPenaltyWeight)+abs($tardinessPenaltyWeight));
    }

    protected function firstTryRight(Job $job): float {
        $processTime = $job->getProcessTime();
        $tardinessPenalty = $job->getTardinessPenalty();
        $processTimeWeight = 2;
        $tardinessPenaltyWeight = -6;
        return (($processTimeWeight*$processTime)+($tardinessPenaltyWeight*$tardinessPenalty))/(abs($processTimeWeight)+abs($tardinessPenaltyWeight));
    }
}