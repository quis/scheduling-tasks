<?php

namespace Quis\Ptsz\Algorithms\Abstracts;

use Quis\Ptsz\Data\Result;
use Quis\Ptsz\Data\Instance;
use Quis\Ptsz\Data\Timeline;
use Quis\Ptsz\Exceptions\AlgorithmException;
use Quis\Ptsz\Exceptions\MethodNotFoundException;

abstract class Algorithm
{
    /**
     * @var Instance
     */
    protected $instance;
    /**
     * @var float
     */
    protected $h;

    /**
     * @var Result
     */
    protected $result;

    /**
     * @var Timeline
     */
    protected $timeline;

    public function setInstance(Instance $instance): self {
        $this->instance = $instance;
        return $this;
    }
    public function getInstance(): Instance {
        return $this->instance;
    }
    public function setHCoefficient(float $h): self {
        $this->h = $h;
        return $this;
    }
    public function getHCoefficient(): float {
        return $this->h;
    }
    protected function prepareJobListWithHeurestics(string $method): array{
        if(!method_exists($this, $method)) {
            throw new MethodNotFoundException("Method $method not found in algorithm ". __CLASS__);
        }

        $result = [];
        $jobList = $this->instance->getJobList();
        foreach($jobList as $job) {
            $result[] = [
                'heurestics' => $this->$method($job),
                'job' => $job
            ];
        }
        return $result;
    }

    protected function sortByHeurestics(string $method, int $dir = SORT_ASC): array {
        $result = $this->prepareJobListWithHeurestics($method);

        $sorted = usort($result, function(array $a, array $b) use ($dir) : int {
            if ($a['heurestics'] === $b['heurestics']) {
                return 0;
            }
            if($dir === SORT_ASC) {
                return ($a['heurestics'] < $b['heurestics']) ? -1 : 1;
            } else {
                return ($a['heurestics'] > $b['heurestics']) ? -1 : 1;
            }

        });
        if(!$sorted) {
            $this->throwException("Sorting by heurestic $method failed in algorithm ". __CLASS__);
        }
        return $result;
    }
    protected function throwException(string $message) {
        $ex = new AlgorithmException($message);
        $ex->setInstance($this->instance)->setAlgorithm($this);
        throw $ex;
    }
    protected function isNotUsedJobsLeft(): bool {
        $jobList = $this->instance->getJobList();
        foreach($jobList as $job) {
            if(!$this->timeline->isJobUsed($job)) {
                return true;
            }
        }
        return false;
    }
    protected function prepareResultAndTimeline() {
        $due = $this->instance->getCommonDueDate($this->h);
        $this->result = new Result();
        $this->timeline = new Timeline();
        $this->timeline->setCommonDueDate($due);
        $this->result
            ->setTimeline($this->timeline)
            ->setHCoefficient($this->h)
            ->setSumOfProcessingTimes($this->instance->getProcessingTimesSum())
            ->setCommonDueDate($due)
            ->setSourceInstance($this->instance);
    }

    abstract public function process(): Result;
}