<?php

namespace Quis\Ptsz;

use Quis\Ptsz\Exceptions\ConfigParamNotFoundException;

class Configuration
{
    private static $instance;

    private $params = [
        'data_path' => 'data',
        'results_path' => 'data/results',
        'algorithms_path' => 'src/Algorithms',
        'algorithms_namespace' => '\\Quis\\Ptsz\\Algorithms\\',
        'templates_path' => 'resources/templates/',
        'html_results_path' => 'data/html_results',
    ];

    private function __construct()
    {
    }

    /**
     * @param string $param param to get from configuration
     * @return mixed param value
     * @throws ConfigParamNotFoundException
     */
    public function getParam(string $param) {
        if(!isset($this->params[$param])) {
            throw new ConfigParamNotFoundException("Param $param not found");
        }
        return $this->params[$param];
    }

    public static function getInstance() : self {
        if(!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}