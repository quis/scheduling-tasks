<?php

namespace Quis\Ptsz\Data;


use Quis\Ptsz\Exceptions\JobNotFoundException;

class Instance {
    protected $jobList = [];
    protected $num;
    protected $plannedJobsCount;

    public function __construct(int $num) {
        $this->num = $num;
    }

    public function add(Job $job) {
        $this->jobList[] = $job;
    }

    public function getNum() {
        return $this->num;
    }

    public function getPlannedJobsCount() {
        return $this->plannedJobsCount;
    }

    public function setPlannedJobsCount(int $plannedJobsCount) {
        $this->plannedJobsCount = $plannedJobsCount;
        return $this;
    }

    public function getJobsCount(): int {
        return count($this->jobList);
    }

    public function getJobList(): array {
        return $this->jobList;
    }

    public function getProcessingTimesSum(): int {
        $sum = 0;
        foreach($this->jobList as $job) {
            $sum += $job->getProcessTime();
        }
        return $sum;
    }

    public function getCommonDueDate(float $h): int {
        $sum = $this->getProcessingTimesSum();
        return floor($sum*$h);
    }

    public function getJobById(int $id): Job {
        foreach($this->getJobList() as $job) {
            if($job->getId() === $id) {
                return $job;
            }
        }
        $ex = new JobNotFoundException("Job not found");
        $ex->setJobId($id);
        throw $ex;
    }

    public function getJobByData(array $data): Job {
        foreach($this->getJobList() as $job) {
            if($job->getProcessTime() === $data[0] && $job->getEarlinessPenalty() === $data[1] && $job->getTardinessPenalty() === $data[2]) {
                return $job;
            }
        }
        $ex = new JobNotFoundException("Job not found");
        $ex->setJobData($data);
        throw $ex;
    }
}