<?php

namespace Quis\Ptsz\Data;


class TimelineJob
{
    protected $job;
    protected $startTime;
    protected $used;
    protected $timeline;

    public function setTimeline(Timeline $timeline): self {
        $this->timeline = $timeline;
        return $this;
    }

    public function getTimeline(): Timeline {
        return $this->timeline;
    }

    public function getPenalty(): int {
        return $this->getTimeline()->getPenaltyForEndTime($this->getJob(), $this->getEndTime());
    }

    public function setJob(Job $job): self {
        $this->job = $job;
        return $this;
    }

    public function getJob(): Job {
        return $this->job;
    }

    public function setStartTime(int $time): self {
        $this->startTime = $time;
        return $this;
    }

    public function getStartTime(): int {
        return $this->startTime;
    }

    public function getEndTime(): int {
        return $this->startTime + $this->job->getProcessTime();
    }

    public function setUsed(): self {
        $this->used = true;
        return $this;
    }

    public function isUsed(): bool {
        return $this->used;
    }
}