<?php

namespace Quis\Ptsz\Data;


use Quis\Ptsz\DataManagment\LoadedData;

class ResultSet
{
    /**
     * @var array
     */
    protected $results = [];
    /**
     * @var LoadedData
     */
    protected $sourceData;
    /**
     * @var float
     */
    protected $hCoefficient;

    public function add(Result $result): self {
        $this->results[] = $result;
        return $this;
    }
    public function setSourceData(LoadedData $sourceData): self {
        $this->sourceData = $sourceData;
        return $this;
    }
    public function getSourceData(): LoadedData {
        return $this->sourceData;
    }
    public function getResults(): array {
        return $this->results;
    }
    public function setHCoefficient(float $hCoefficient): self {
        $this->hCoefficient = $hCoefficient;
        return $this;
    }
    public function getHCoefficient(): float {
        return $this->hCoefficient;
    }
}