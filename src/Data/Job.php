<?php

namespace Quis\Ptsz\Data;

class Job
{
    protected $id;
    protected $processTime;
    protected $earlinessPenalty;
    protected $tardinessPenalty;

    public function __construct(int $id, int $processTime, int $earlinessPenalty, int $tardinessPenalty) {
        $this->id = $id;
        $this->processTime = $processTime;
        $this->earlinessPenalty = $earlinessPenalty;
        $this->tardinessPenalty = $tardinessPenalty;
    }

    public function getId(): int {
        return $this->id;
    }
    public function getProcessTime() {
        return $this->processTime;
    }
    public function getEarlinessPenalty() {
        return $this->earlinessPenalty;
    }
    public function getTardinessPenalty() {
        return $this->tardinessPenalty;
    }
}