<?php

namespace Quis\Ptsz\Data;


use Quis\Ptsz\Exceptions\TimelineException;

class Timeline
{
    protected $timelineJobs = [];
    protected $commonDueDate;
    protected $penaltiesSum = 0;
    protected $jobsSorted = false;

    public function setJob(int $time, Job $job): ?TimelineJob {
        if($time < 0) {
            return null;
        }
        $timelineJob = new TimelineJob();
        $timelineJob->setTimeline($this)->setStartTime($time)->setJob($job)->setUsed();
        $this->timelineJobs[] = $timelineJob;
        $this->jobsSorted = false;
        $this->penaltiesSum += $timelineJob->getPenalty();
        return $timelineJob;
    }

    public function getPenaltiesSum(): int {
        return $this->penaltiesSum;
    }

    public function isJobUsed(Job $job): bool {
        foreach($this->timelineJobs as $timelineJob) {
            if($timelineJob->getJob() === $job) {
                return $timelineJob->isUsed();
            }
        }
        return false;
    }

    public function setCommonDueDate(int $commonDueDate): self {
        $this->commonDueDate = $commonDueDate;
        return $this;
    }

    public function getCommonDueDate(): int {
        return $this->commonDueDate;
    }

    public function getPossibleTimeAtEnd(): int {
        try {
            $lastJob = $this->getLastTimelineJob();
        } catch (TimelineException $ex) {
            return 0;
        }
        return $lastJob->getEndTime();
    }

    public function getFirstTimelineJob(): TimelineJob {
        $currentStart = -1;
        foreach($this->timelineJobs as $timelineJob) {
            if($currentStart === -1 || $timelineJob->getStartTime() < $currentStart) {
                $currentStart = $timelineJob->getStartTime();
                $result = $timelineJob;
            }
        }
        if(empty($result)) {
            throw new TimelineException("First job not found - timeline is empty");
        }
        return $result;
    }

    public function getLastTimelineJob(): TimelineJob {
        $currentEnd = -1;
        foreach($this->timelineJobs as $timelineJob) {
            if($currentEnd === -1 || $timelineJob->getEndTime() > $currentEnd) {
                $currentEnd = $timelineJob->getEndTime();
                $result = $timelineJob;
            }
        }
        if(empty($result)) {
            throw new TimelineException("Last job not found - timeline is empty");
        }
        return $result;
    }

    public function getPenaltyForEndTime(Job $job, int $endTime) {
        $due = $this->getCommonDueDate();
        $diff = $due-$endTime;
        if($diff > 0) {
            $earlinessPenalty = $job->getEarlinessPenalty();
            return $earlinessPenalty*$diff;
        } else if($diff < 0) {
            $diff = abs($diff);
            $tardinessPenalty = $job->getTardinessPenalty();
            return $tardinessPenalty*$diff;
        }
        return 0;
    }

    protected function getPossibleJobEndTimeForStartTime(Job $job, int $startTime): int {
        $endTime = $startTime + $job->getProcessTime();
        return $endTime;
    }

    public function tryToPutJobBeforeFirstJob(Job $job): bool {
        $firstTimelineJob = $this->getFirstTimelineJob();
        $firstJobStartTime = $firstTimelineJob->getStartTime();

        $startTime = $firstJobStartTime-$job->getProcessTime();
        if($startTime > 0) {
            $this->setJob($startTime, $job);
            return true;
        }
        return false;
    }

    public function sortJobs(): array {
        if(!$this->jobsSorted) {
            $this->jobsSorted = uasort($this->timelineJobs, function (TimelineJob $a, TimelineJob $b): int {
                if ($a->getStartTime() === $b->getStartTime()) {
                    return 0;
                }
                return ($a->getStartTime() < $b->getStartTime()) ? -1 : 1;
            });
        }
        if(!$this->jobsSorted) {
            throw new TimelineException("Can not sort jobs by start time");
        }
        return $this->timelineJobs;
    }
}