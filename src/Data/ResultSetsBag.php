<?php

namespace Quis\Ptsz\Data;


class ResultSetsBag
{
    protected $resultSets = [];
    protected $fileName;

    public function add(ResultSet $resultSet) {
        $this->resultSets[] = $resultSet;
    }
    public function getResultSets(): array {
        return $this->resultSets;
    }
    public function setSourceFileName(string $fileName) : self {
        $this->fileName = $fileName;
        return $this;
    }
    public function getSourceFileName(): string {
        return $this->fileName;
    }

}