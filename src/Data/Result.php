<?php

namespace Quis\Ptsz\Data;


class Result
{
    protected $h;
    protected $commonDueDate;
    protected $sumP;
    protected $timeline;
    protected $sourceInstance;
    protected $processingTime;

    public function setSourceInstance(Instance $instance): self {
        $this->sourceInstance = $instance;
        return $this;
    }
    public function getSourceInstance(): Instance {
        return $this->sourceInstance;
    }
    public function setTimeline(Timeline $timeline): self {
        $this->timeline = $timeline;
        return $this;
    }
    public function getHCoefficient(): float {
        return $this->h;
    }
    public function setHCoefficient(float $h): self {
        $this->h = $h;
        return $this;
    }
    public function getCommonDueDate(): int {
        return $this->commonDueDate;
    }
    public function setCommonDueDate(int $commonDueDate): self {
        $this->commonDueDate = $commonDueDate;
        return $this;
    }
    public function getSumOfProcessingTimes(): int {
        return $this->sumP;
    }
    public function setSumOfProcessingTimes(int $sumP): self {
        $this->sumP = $sumP;
        return $this;
    }
    public function getTimeline(): Timeline {
        return $this->timeline;
    }
    public function getProcessingTime(): float {
        return $this->processingTime;
    }
    public function setProcessingTime(float $processingTime): self {
        $this->processingTime = $processingTime;
        return $this;
    }

}