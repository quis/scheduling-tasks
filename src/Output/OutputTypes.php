<?php

namespace Quis\Ptsz\Output;


class OutputTypes
{
    const OUTPUT_FILE = 'file';
    const OUTPUT_HTML = 'html';
}