<?php

namespace Quis\Ptsz\Output;


use Quis\Ptsz\Configuration;
use Quis\Ptsz\Data\Result;
use Quis\Ptsz\Data\ResultSet;
use Quis\Ptsz\Exceptions\InvalidOutputTypeException;
use Quis\Ptsz\Exceptions\OutputException;
use Quis\Ptsz\Utils\FileNameCleaner;

class ResultSetOutputter
{
    /**
     * @var Configuration
     */
    protected $config;
    protected $sourceFile;

    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    public function setSourceFile(string $fileName): self {
        $this->sourceFile = FileNameCleaner::truncateFileExtensionAndPath($fileName);
        return $this;
    }

    public function getSourceFile(): string {
        return $this->sourceFile;
    }

    public function output(ResultSet $resultSet) {
        $results = $resultSet->getResults();
        $this->setSourceFile($resultSet->getSourceData()->getFileName());

        foreach($results as $result) {
            $finalFileName = $this->getFileNameForResult($result);
            $this->outputResultToFile($result, $finalFileName);
        }
    }

    public function getFileNameForResult(Result $result): string {
        $h = $result->getHCoefficient();
        $hFormatted = $this->prepareHCoefficient($h);
        $instanceNum = (string) $result->getSourceInstance()->getNum();
        return implode('_', [$this->getSourceFile(), $hFormatted, $instanceNum]) .'.txt';
    }

    public function outputResultToFile(Result $result, string $file) {
        $resultsPath = $this->config->getParam('results_path');
        if(!file_exists($resultsPath)) {
            mkdir($resultsPath);
        }

        $resultPath = $resultsPath .'/'. $file;

        $timeline = $result->getTimeline();
        $timelineJobs = $timeline->sortJobs();

        $file = fopen($resultPath, 'w');
        if(!$file) {
            throw new OutputException("Can not create file $resultPath to output result");
        }

        print("Saving file $resultPath with results... ");
        $penaltiesSum = (string) $timeline->getPenaltiesSum();
        $firstJobStartTime = (string) $timeline->getFirstTimelineJob()->getStartTime();
        $commonDueDate = (string) $result->getCommonDueDate();
        fwrite($file, "$penaltiesSum $firstJobStartTime $commonDueDate ");
        $ids = [];
        foreach($timelineJobs as $timelineJob) {
            $job = $timelineJob->getJob();
            $ids[] = implode(' ', [ $job->getProcessTime(), $job->getEarlinessPenalty(), $job->getTardinessPenalty() ]);
        }
        fwrite($file, implode(' ', $ids));
        fclose($file);
        print("done!". PHP_EOL);
    }

    protected function prepareHCoefficient(float $h): string {
        return preg_replace('/[^1-9]+/', '', (string) $h);;
    }
}