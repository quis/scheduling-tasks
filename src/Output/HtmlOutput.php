<?php

namespace Quis\Ptsz\Output;


use Quis\Ptsz\Configuration;
use Quis\Ptsz\Data\OptimalPenaltiesSumData;
use Quis\Ptsz\Data\ResultSetsBag;
use Quis\Ptsz\Exceptions\TemplateNotFoundException;

class HtmlOutput
{
    /**
     * @var Configuration
     */
    protected $config;

    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    public function output(ResultSetsBag $resultSetsBag): void {
        $tableTemplate = $this->getTemplate('table');
        $rowTemplate = $this->getTemplate('row');
        $resultSets = $resultSetsBag->getResultSets();
        $content = '';

        foreach($resultSets as $idx => $resultSet) {
            $rows = '';
            $avgApproximateErrors = [];

            $results = $resultSet->getResults();
            foreach($results as $result) {
                $timeline = $result->getTimeline();
                $instance = $result->getSourceInstance();
                $jobsCount = $instance->getPlannedJobsCount();
                $instanceNum = $instance->getNum();
                $penaltiesSum = $timeline->getPenaltiesSum();
                $optimalPenaltiesSum = OptimalPenaltiesSumData::getOptimalPenaltiesSum($jobsCount, $instanceNum, $result->getHCoefficient());
                $approximateError = (($optimalPenaltiesSum-$penaltiesSum)/$optimalPenaltiesSum)*100;
                $avgApproximateErrors[] = $approximateError;

                $rowData = [
                    'instanceNum' => $instanceNum,
                    'startTime' => $timeline->getFirstTimelineJob()->getStartTime(),
                    'processingTime' => $result->getProcessingTime(),
                    'penaltiesSum' => $penaltiesSum,
                    'optimalPenaltiesSum' => $optimalPenaltiesSum,
                    'approximateError' => $approximateError
                ];
                $rows .= $this->fillTemplateWithData($rowTemplate, $rowData);
            }
            $sourceData = $resultSet->getSourceData();
            $avgApproximateErrorSum = 0.0;
            foreach($avgApproximateErrors as $approximateError) {
                $avgApproximateErrorSum += $approximateError;
            }
            $avgApproximateErrorCount = count($avgApproximateErrors);
            $avgError = ($avgApproximateErrorSum/$avgApproximateErrorCount);
            $variance = 0.0;
            foreach($avgApproximateErrors as $approximateError) {
                $variance += ($approximateError-$avgError) ** 2;
            }
            $variance /= $avgApproximateErrorCount;

            $tableData = [
                'rows' => $rows,
                'fileName' => $sourceData->getFileName(),
                'jobsCount' => $sourceData->getPlannedInstances(),
                'hCoefficient' => $resultSet->getHCoefficient(),
                'avgError' => $avgError,
                'standardDeviation' => sqrt($variance),
            ];
            $content .= $this->fillTemplateWithData($tableTemplate, $tableData);
        }

        $pageTemplate = $this->getTemplate('page');
        $resultsDirectory = $this->config->getParam('html_results_path');
        if(!file_exists($resultsDirectory)) {
            mkdir($resultsDirectory);
        }

        $file = $this->getFileForResults($resultSetsBag);
        $html = $this->fillTemplateWithData($pageTemplate, [
            'content' => $content
        ]);
        file_put_contents($file, $html);
    }

    protected function getTemplate(string $templateName): string {
        $fileName = $this->getTemplateFileName($templateName);
        if(!file_exists($fileName)) {
            throw new TemplateNotFoundException("Template {$templateName} not found");
        }
        return file_get_contents($fileName);
    }

    protected function getTemplateFileName(string $templateName) {
        return $this->config->getParam('templates_path') . $templateName .'.html';
    }

    protected function fillTemplateWithData(string $template, array $data): string {
        $result = $template;
        foreach($data as $key => $value) {
            $replaceKey = '{{'. $key .'}}';
            if(is_float($value)) {
                $value = round($value, 2);
            }
            $result = str_replace($replaceKey, $value, $result);
        }
        return $result;
    }

    protected function getFileForResults(ResultSetsBag $resultSetsBag) {
        $sourceFileName = $resultSetsBag->getSourceFileName();
        $sourceFileNameArray = explode('.', $sourceFileName);
        array_pop($sourceFileNameArray);
        $sourceFileNameArray = explode('/', implode('.', $sourceFileNameArray));
        $resultsDirectory = $this->config->getParam('html_results_path');
        return $resultsDirectory .'/'. array_pop($sourceFileNameArray) .'_results.html';
    }
}